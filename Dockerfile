FROM openjdk

COPY /workspace/source/initial/target/ /app

WORKDIR /app

CMD java -jar gs-maven-*